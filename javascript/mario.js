Q.animations("mario_anim", {
	caminar : {
		frames : [4, 5, 8],
		rate : 1 / 6,
		loop : false
	},
	quieto : {
		frames : [1],
		rate : 1 / 2,
		loop : false
	},
	saltar : {
		frames : [2],
		rate : 1 / 2,
		loop : false
	},
	morir : {
		frames : [12],
		rate : 1,
		loop : false,
		trigger : "muerto"
	}
});

Q.Sprite.extend("Mario", {
	init : function(p) {

		this._super(p, {
			sheet : "mario_enano",
			sprite : "mario_anim",
			frame : 1,
			x : 100,
			y : 50,
			jumpSpeed : -500,
			muerto : false
		});

		//agregamos el componenten tween
		this.add("2d, platformerControls, animation, tween");
		this.on("bump.left, bump.right, bump.top", function(colision) {

			if (colision.obj.isA("Goomba") || colision.obj.isA("TortugaVerdeAlada") || colision.obj.isA("TortugaVerde")) {

				Q.audio.stop("tema_superficie.mp3");
				Q.audio.play("mario_muere.mp3", {
					debounce : 3000
				});

				this.p.muerto = true;
				this.del("platformerControls, 2d");
				this.play("morir");
			}

		});

		this.on("muerto", function() {

			this.p.escena.unfollow();

			//guardamos una referencia al mismo objeto
			//vea detalle_this.html
			var self = this;
			//animate recibe 3 argumentos:
			//animate(obj1,tiempoAnimacion, objCallback)
			//en obj1 se definen las propiedades finales que alcanzara
			//en objCallback se define basicamente la propiedad callback
			//que permite definir una funcion que se ejecutara al terminar
			//la animacion del tween
			this.animate({
				y : (this.p.y - 100)
			}, 0.5, {
				callback : function() {

					self.animate({
						y : (self.p.altoLayout)
					}, 0.5, {
						callback : function() {
							self.destroy();
							
							Q.stageScene("finJuego", 1, {
								label : "Recuerda inscribirte :)\nfacebook.com/HTML5GamesDev"
							});
						}
					});
				}
			});
		});

	},
	step : function() {

		if (this.p.muerto === false) {

			if (this.p.vx > 0 && this.p.vy === 0) {

				this.p.flip = false;
				this.play("caminar");
			} else if (this.p.vx < 0 && this.p.vy === 0) {

				this.p.flip = "x";
				this.play("caminar");
			} else if (this.p.vx === 0 && this.p.vy === 0) {

				this.play("quieto");
			} else if (this.p.vy !== 0) {

				Q.audio.play("salto_enano.mp3", {
					debounce : 1000
				});
				this.play("saltar");
			}
		}
	}
});
