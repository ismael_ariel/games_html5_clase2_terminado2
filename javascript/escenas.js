Q.scene("escena1", function(stage) {

	var cielo = new Q.TileLayer({
		dataAsset : "mapa_escena1.tmx",
		layerIndex : 0,
		sheet : "escenario",
		type : Q.SPRITE_NONE
	});

	stage.insert(cielo);

	var nubes = new Q.TileLayer({
		dataAsset : "mapa_escena1.tmx",
		layerIndex : 1,
		sheet : "escenario",
		type : Q.SPRITE_NONE
	});
	stage.insert(nubes);

	var colisiones = new Q.TileLayer({
		dataAsset : "mapa_escena1.tmx",
		layerIndex : 2,
		sheet : "escenario"
	});
	stage.collisionLayer(colisiones);

	var alturaPiso = colisiones.p.h - (3 * 32);
	var mario = stage.insert(new Q.Mario({
		x : 50,
		y : alturaPiso,
		altoLayout : colisiones.p.h,
		escena : stage
	}));

	stage.add("viewport").follow(mario, {
		x : true,
		y : true
	}, {
		minX : 32,
		maxX : colisiones.p.w,
		minY : 0,
		maxY : colisiones.p.h
	});

	stage.insert(new Q.Goomba({
		x : 200,
		y : alturaPiso,
		altoLayout : colisiones.p.h
	}));
	stage.insert(new Q.TortugaVerde({
		x : 300,
		y : alturaPiso,
		altoLayout : colisiones.p.h
	}));
	stage.insert(new Q.TortugaVerdeAlada({
		x : 350,
		y : alturaPiso,
		altoLayout : colisiones.p.h
	}));

	Q.audio.play("tema_superficie.mp3", {
		loop : true
	});

});

Q.scene("finJuego", function(stage) {

	//creamos una ventana
	var ventana = new Q.UI.Container({
		//hacemos que ocupe la mitad del escenario a lo alto y ancho
		x : Q.width / 2,
		y : Q.height / 2,
		//ROJO,VERDE, AZUL Y ALFA CHANNEL
		fill : "rgba(0,0,0,0.5)"
	});
	//insertamos la ventana en el escenario
	stage.insert(ventana);

	//insertamos un boton en la ventana
	var boton = ventana.insert(new Q.UI.Button({
		//posicion del boton (con respecto al contenedor)
		x : 0,
		y : 0,
		//RELLENO
		fill : "#CCCCCC",
		//mensaje del boton
		label : "Jugar de Nuevo"
	}));

	var tituloVentana = new Q.UI.Text({
		x : 10,
		//lo ponemos arriba del boton
		y : -10 - boton.p.h,
		label : "perdiste"
	});

	//insertamos en la ventana el titulo
	ventana.insert(tituloVentana);

	//si el boton UI es presionado
	boton.on("click", function() {
		//destruimos todos los escenarios y sus objetos en ellos
		Q.clearStages();
		//arrancamos de nuevo el nivel 1
		Q.stageScene('escena1');
	});

	ventana.fit(20);
});
