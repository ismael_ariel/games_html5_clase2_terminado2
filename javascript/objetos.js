Q.Sprite.extend("CajaMovible", {
	init : function(p) {
		this._super(p, {
			sheet : "escenario",
			frame:62,
			gravity:0
		});
		this.add("2d, animation, tween");

		this.on("bump.bottom", function(colision) {
			
			if (colision.obj.isA("Mario")) {

				var self = this;
				var posicionOriginal = this.p.y;
				this.animate({
					y : (this.p.y - 20)
				}, 0.5, {
					callback : function() {

						self.animate({
							y : posicionOriginal
						}, 0.5);
					}
				});

			}

		});
	}
});
