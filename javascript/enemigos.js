Q.animations("goomba_anim", {
	caminar : {
		frames : [1, 0],
		rate : 1 / 4
	},
	aplastar : {
		frames : [3],
		rate : 1,
		loop : false,
		trigger : "aplastado"
	}
});

Q.animations("tortuga_verde_anim", {
	caminar : {
		frames : [0, 1],
		rate : 1 / 4,
		loop : false
	},
	concha : {
		frames : [2, 4],
		rate : 1 / 2,
		loop : false
	}
});

//ANIMACION TORTUGA VERDE ALADA(TAREA1)
Q.animations("tortuga_verde_alada_anim", {
	caminar : {
		frames : [5, 6],
		rate : 1 / 4,
		loop : false
	},
	concha : {
		frames : [2, 4],
		rate : 1 / 2,
		loop : false
	}
});

//DEFINICION TORTUGA VERDE ALADA (TAREA 1)
Q.Sprite.extend("TortugaVerdeAlada", {
	init : function(p) {

		this._super(p, {
			sheet : "enemigos_altos",
			sprite : "tortuga_verde_alada_anim", //note que usamos una animacion que definimos
			x : 200,
			y : 40,
			vx : 120,
			frame : 0,
			esConcha : false
		});

		//HABILITAMOS LOS MODULOS PARA QUE LA TORTUGA TENGA ANIMACIONES Y REBOTE
		this.add("2d, aiBounce, animation,tween");
		this.on("bump.top", function(colision) {

			if (colision.obj.isA("Mario")) {

				this.p.vx = 300;
				Q.audio.play("patada.mp3");

				colision.obj.p.vy = -300;
				//nos cambiamos de hoja de mosaicos
				this.sheet("enemigos_bajos", true);
				//cambiamos la bandera
				this.p.esConcha = true;
				//ejecutamos una animacion
				this.play("concha");

			}

		});

	},
	step : function() {

		if (this.p.esConcha === false) {
			//invertimos la animacion dependiendo de si esta caminando a la derecha o la izquierda
			if (this.p.vx > 0) {

				this.p.flip = "x";
				this.play("caminar");

			} else if (this.p.vx < 0) {

				this.p.flip = false;
				this.play("caminar");

			}
		}
	},
	choqueConcha : function() {

		this.del("2d");
		this.animate({
			y : this.p.altoLayout + 20,
			x : (this.p.x + 30)
		}, 0.5, {
			callback : function() {
				
				this.destroy();
			}
		});

	}
});

Q.Sprite.extend("Goomba", {
	init : function(p) {

		this._super(p, {
			sheet : "enemigos_bajos",
			sprite : "goomba_anim",
			x : 110,
			y : 40,
			vx : 120,
			frame : 0,
			jumpSpeed : -300
		});

		this.add("2d, aiBounce, animation, tween");
		this.play("caminar");

		this.on("bump.top", function(colision) {

			//destruimos al goomba si lo aplastan
			if (colision.obj.isA("Mario")) {

				colision.obj.p.vy = -300;

				Q.audio.play("patada.mp3");
				//hacemos saltar al mario
				colision.obj.p.vy = -300;
				//detenemos al goomba
				this.p.vx = 0;
				//iniciamos una animacions
				this.play("aplastar");
			}
		});

		this.on("aplastado", function() {
			//una vez que termine la animacion del goomba aplastado
			//destruimos al goomba
			this.destroy();
		});

		//forma alternativa de escuchar eventos:
		//this.on("bump.top", this, "colisionArriba");
	},
	colisionArriba : function(colision) {

		//destruimos al goomba si lo aplastan
		if (colision.obj.isA("Mario")) {
			this.destroy();
		}
	},
	choqueConcha : function() {

		this.del("2d, aiBounce");
		this.stop();
		this.animate({
			y : this.p.altoLayout + 20,
			x : (this.p.x + 30)
		}, 0.5, {
			callback : function() {
				this.destroy();
			}
		});

	}
});

Q.Sprite.extend("TortugaVerde", {
	init : function(p) {

		this._super(p, {
			sheet : "enemigos_altos",
			sprite : "tortuga_verde_anim",
			x : 180,
			y : 40,
			vx : 120,
			frame : 0,
			jumpSpeed : -300,
			esConcha : false
		});

		this.add("2d, aiBounce,animation,tween");

		this.on("bump.left, bump.right", function(colision) {

			if (this.p.esConcha && (colision.obj.isA("TortugaVerde") || colision.obj.isA("TortugaVerdeAlada") || colision.obj.isA("Goomba"))) {

				Q.audio.play("patada.mp3");
				colision.obj.choqueConcha();
			}

		});

		this.on("bump.top", function(colision) {

			if (colision.obj.isA("Mario")) {

				this.p.vx = 300;
				Q.audio.play("patada.mp3");

				colision.obj.p.vy = -300;
				//nos cambiamos de hoja de mosaicos
				this.sheet("enemigos_bajos", true);
				//cambiamos la bandera
				this.p.esConcha = true;
				//ejecutamos una animacion
				this.play("concha");

			}

		});
	},
	step : function() {

		if (this.p.esConcha === false) {

			if (this.p.vx > 0) {

				this.p.flip = "x";
				this.play("caminar");
			} else if (this.p.vx < 0) {

				this.p.flip = false;
				this.play("caminar");
			}
		}

	},
	choqueConcha : function() {

		this.del("2d");
		this.stop();
		this.animate({
			y : this.p.altoLayout + 20,
			x : (this.p.x + 30)
		}, 0.5, {
			callback : function() {
				this.destroy();
			}
		});

	}
});
